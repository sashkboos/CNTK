# ConvNet applied on CIFAR-10 dataset, with data augmentation (translation and flipping).

command = TrainConvNet:Eval

precision = "float"; traceLevel = 1 ; deviceId = "auto"

rootDir = "../../.." ; configDir = "./" ; dataDir = "$rootDir$/DataSets/CIFAR-10" ;
outputDir = "./Output" ;

modelPath = "$outputDir$/Models/ResNet20_CIFAR10_DataAug"
#stderr = "$outputDir$/ResNet20_CIFAR10_DataAug_bs_out"

TrainConvNet = {
    action = "train"
    BrainScriptNetworkBuilder = {
        include ".//Macros.bs"
        imageShape = 32:32:3
        labelDim = 10
        featScale = 1/256
        Normalize{f} = x => f .* x
        cMap = 16:32:64
        bnTimeConst = 4096
        numLayers = 3
        model = Sequential (
            Normalize {featScale} :
            ConvBNReLULayer {cMap[0], (3:3), (1:1), bnTimeConst} :
            ResNetBasicStack {numLayers, cMap[0], bnTimeConst} :
            ResNetBasicInc {cMap[1], (2:2), bnTimeConst} :
            ResNetBasicStack {numLayers-1, cMap[1], bnTimeConst} :
            ResNetBasicInc {cMap[2], (2:2), bnTimeConst} :
            ResNetBasicStack {numLayers-1, cMap[2], bnTimeConst} :
            AveragePoolingLayer {(8: 8), stride = 1} :
            LinearLayer {labelDim}
        )
        features = Input {imageShape}
        labels   = Input {labelDim}
        z = model (features)
        ce       = CrossEntropyWithSoftmax     (labels, z)
        errs     = ClassificationError         (labels, z)
        top5Errs = ClassificationError         (labels, z, topN=5)  
        featureNodes    = (features)
        labelNodes      = (labels)
        criterionNodes  = (ce)
        evaluationNodes = (errs)  
        outputNodes     = (z)
    }
    SGD = {
        epochSize = 0
        minibatchSize = 128
        learningRatesPerMB = 1.0*80:0.1*40:0.01
        momentumPerMB = 0.9
        maxEpochs = 10
        L2RegWeight = 0.0001
        numMBsToShowResult = 100
        ParallelTrain = [
            distributedMBReading = true
            parallelizationMethod = "DataParallelSGD"
            syncPerfStats = 10
            DataParallelSGD = [
                gradientBits = 8
                numElementsPerBucket = 128
                scalingFactor = max
            ]
            AutoAdjust = [
                autoAdjustMinibatch = true       
                minibatchSizeTuningFrequency = 3 
            ]
        ]
    }
    reader = {
        verbosity = 0 ; randomize = true
        deserializers = ({
            type = "ImageDeserializer" ; module = "ImageReader"
            file = "../../../DataSets/CIFAR-10/train_map.txt"
            input = {
                features = { transforms = (
                    { type = "Crop" ; cropType = "RandomSide" ; sideRatio = 0.8 ; jitterType = "UniRatio" } :
                    { type = "Scale" ; width = 32 ; height = 32 ; channels = 3 ; interpolations = "linear" } :
                    { type = "Mean" ; meanFile = "../../../DataSets/CIFAR-10/CIFAR-10_mean.xml" } : 
                    { type = "Transpose" }
                )}
                labels = { labelDim = 10 }
            }
        })
    }

    cvReader = {
        verbosity = 0 ; randomize = false
        deserializers = ({
            type = "ImageDeserializer" ; module = "ImageReader"
            file = "$dataDir$/test_map.txt"
            input = {
                features = { transforms = (
                   { type = "Scale" ; width = 32 ; height = 32 ; channels = 3 ; interpolations = "linear" } :
                   { type = "Mean"; meanFile = "$dataDir$/CIFAR-10_mean.xml" } : 
                   { type = "Transpose" }
                )}
                labels = { labelDim = 10 }
            }
        })
    }
}

# Eval action
Eval = {
    action = "eval"
    evalNodeNames = errs:top5Errs  # also test top-5 error rate
    # Set minibatch size for testing.
    minibatchSize = 128

    reader = {
        verbosity = 0 ; randomize = false
        deserializers = ({
            type = "ImageDeserializer" ; module = "ImageReader"
            file = "$dataDir$/test_map.txt"
            input = {
                features = { transforms = (
                   { type = "Scale" ; width = 32 ; height = 32 ; channels = 3 ; interpolations = "linear" } :
                   { type = "Mean"; meanFile = "$dataDir$/CIFAR-10_mean.xml" } : 
                   { type = "Transpose" }
                )}
                labels = { labelDim = 10 }
            }
        })
    }
}
