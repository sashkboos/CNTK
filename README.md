<h1 align="center">SparCML: High-Performance Sparse Communication for Machine Learning</h1>


This is the implementation of SparCML on **The Microsoft Cognitive Toolkit (CNTK)**. Other implementations will be released soon.



# What is The Microsoft Cognitive Toolkit

The Microsoft Cognitive Toolkit (https://www.microsoft.com/en-us/research/product/cognitive-toolkit/), is a unified deep-learning toolkit that describes neural networks as a series of computational steps via a directed graph. In this directed graph, leaf nodes represent input values or network parameters, while other nodes represent matrix operations upon their inputs. CNTK allows to easily realize and combine popular model types such as feed-forward DNNs, convolutional nets (CNNs), and recurrent networks (RNNs/LSTMs). It implements stochastic gradient descent (SGD, error backpropagation) learning with automatic differentiation and parallelization across multiple GPUs and servers. CNTK has been available under an open-source license since April 2015. It is our hope that the community will take advantage of CNTK to share ideas more quickly through the exchange of open source working code.

Wiki: Go to the [CNTK Wiki](https://github.com/Microsoft/CNTK/wiki) for all information on CNTK including [setup](https://github.com/Microsoft/CNTK/wiki/Setup-CNTK-on-your-machine ), [examples](https://github.com/Microsoft/CNTK/wiki/Examples ), etc.

License: See [LICENSE.md](./LICENSE.md) in the root of this repository for the full license information.

Tutorial: [Microsoft Computational Network Toolkit (CNTK) @ NIPS 2015 Workshops](https://research.microsoft.com/en-us/um/people/dongyu/CNTK-Tutorial-NIPS2015.pdf)

Blogs:

* [Microsoft Computational Network Toolkit offers most efficient distributed deep learning computational performance](https://blogs.technet.com/b/inside_microsoft_research/archive/2015/12/07/microsoft-computational-network-toolkit-offers-most-efficient-distributed-deep-learning-computational-performance.aspx)
* [Microsoft researchers win ImageNet computer vision challenge (December 2015)](https://blogs.microsoft.com/next/2015/12/10/microsoft-researchers-win-imagenet-computer-vision-challenge/)




# Requirements

 - **CUDA 8 (or later)**
 - **Open MPI** : Use [this](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-cntk-on-linux#open-mpi) instruction.
 - **Protobuf** :  Use [this](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-cntk-on-linux#protobuf) instruction.
 - **ZLIB/LIBZIP** : Use [this](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-cntk-on-linux#zlib) instruction.
 - **Boost Library** : Use [this](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-cntk-on-linux#boost-library) instruction.
 - **OpenCV** : Use [this](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-opencv-on-linux) instruction.
 - **Custom MKL/CUB-1.4.1/CuDNN-5.1** : You do not need to install separately. Use **Dependencies/** directory.


# Installation

 1. Clone the repo using `git clone https://gitlab.com/sashkboos/CNTK.git & cd CNTK/`
 2. Update submodules using `git submodule update --recursive --init`
 3. Create a build directory using `mkdir -p build/release & cd build/release`
 4. Configure the package using the following:

     `../../configure --with-mkl=\PATH\TO\CNTK/Dependencies/mkl --with-cuda=\PATH\TO\CUDA --with-cub=\PATH\TO\CNTK/Dependencies/cub-1.4.1  --with-cudnn=\PATH\TO\CNTK/Dependencies/cudnn-5.1
     --with-gdk-nvml-lib=\PATH\TO\GDK\NVML\LIB  --with-gdk-include=\PATH\TO\GDK\NVML\INCLUDE `
     
5. Make the project using `make -j all`.


# Usage

You can use the **Topk** with ParallelTrain in your brainscript file as follow:

          ParallelTrain = {  
          parallelizationMethod = "DataParallelSGD"  
          distributedMBReading = true  
          parallelizationStartEpoch = 1  
          DataParallelSGD = {  
              gradientBits = 4  # Number of quantization bits
              topK=16           # Top-16 (over 512 elements)
          }  
      }

 **Note**: Setting gradientBits=32 will run vanilla sgd regardless of the topk value.


# Citation

You can cite the following work if you found it useful:

```
@article{renggli2018sparcml,
  title={Sparcml: High-performance sparse communication for machine learning},
  author={Renggli, C{\`e}dric and Ashkboos, Saleh and Aghagolzadeh, Mehdi and Alistarh, Dan and Hoefler, Torsten},
  journal={arXiv preprint arXiv:1802.08021},
  year={2018}
}
```
